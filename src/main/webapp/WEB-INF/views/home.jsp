<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
</head>
<body>
<h1>
	Hello world!  
</h1>

<P>  The time on the server is ${serverTime}. </P>

<c:set var="numPage" value="0" />
<c:set var="parPage" value="2" />

	<div>
		<label for="numPage">Numero de Page *</label> 
		<input type="number" id="numPage" name="numPage" />
	</div>
	<div>
		<label for="parPage">Nombre par Page *</label> 
		<input type="number" id="parPage" name="parPage" />
	</div>

<c:url value="//showAllByPage/${numPage}/${parPage}" var = "monlien"/>
<a href="${monlien}"> Par page </a>

</body>
</html>
